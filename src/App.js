import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { ApolloProvider } from '@apollo/react-hooks'

import Home from './containers/Home'
import Login from './containers/Login'
import Register from './containers/Register'
import Dashboard from './containers/Dashboard'
import { OTPModal } from './containers/OTPModal'

import { client } from './index'

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Route path="/" exact component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/dashboard" component={Dashboard} />
      </Router>
      <OTPModal />
    </ApolloProvider>
  )
}

export default App
