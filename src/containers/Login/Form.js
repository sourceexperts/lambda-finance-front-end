import React, { Component } from 'react'
import {
  Form, Icon, Input, Button, Row,
} from 'antd'
import { Link } from 'react-router-dom'

class LoginForm extends Component {
  handleSubmit = (login) => (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        login({ variables: { ...values } })
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form
    return (
      <Row type="flex" justify="center" className="margin-row">
        <Form onSubmit={this.handleSubmit(this.props.login)} className="login-form">
          <h1>Login</h1>
          <Form.Item>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input your username' }],
            })(
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input your password' }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
            )}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Log in
            </Button>
            Or <Link to="/register">register now!</Link>
          </Form.Item>
        </Form>
      </Row>
    )
  }
}

const WrappedLoginForm = Form.create({ name: 'loginForm' })(LoginForm);

export default WrappedLoginForm
