import React from 'react'
import { Redirect } from 'react-router-dom'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks';
import { message } from 'antd'

import Form from './Form'
import './styles.css'

const LOGIN = gql`
  mutation signinUser($username: String!, $password: String!) {
    signinUser(username: $username, password: $password)
  }
`;

const Login = () => {
  const [login, { data, error }] = useMutation(LOGIN)

  if (data) {
    message.success('Successful login!')
    localStorage.setItem('token', data.signinUser)
    return <Redirect to="/dashboard/functions" />
  }

  if (error) {
    message.error(error.message)
  }

  return <Form login={login} />
}

export default Login
