import React, { useEffect, useState } from 'react'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'
import { Modal, Input } from 'antd'

const CONFIRM_OTP = gql`
  mutation confirmOtp($responseId: String!, $tan: String!) {
    confirmOtp(bankId: 1, responseId: $responseId, tan: $tan)
  }
`;

export const OTPModal = ({ visible }) => {
    const [confirmOTP] = useMutation(CONFIRM_OTP)
    const [modalVisible, setModalVisible] = useState(false)
    const [responseId, setResponseId] = useState()
    const [otp, setOtp] = useState('')

    useEffect(() => {
        const optEvents = new EventSource('https://api.lambda.finance/otp');
        optEvents.addEventListener("sse event - mvc", function(e) {
            setModalVisible(true)
            setResponseId(e.data)
        })
    }, [])

    const handleSubmit = () => {
        setModalVisible(false)
        confirmOTP({ variables: { responseId, tan: otp } })
    }

    return (
        <Modal
          title="Payment confirmation"
          visible={modalVisible}
          onOk={handleSubmit}
          onCancel={() => setModalVisible(false)}
          okText="Submit"
        >
            <p>Please type your One Time Password here and press "Submit"</p>
            <Input size="large" value={otp} onChange={(e) => setOtp(e.target.value)} />
        </Modal>
    )
}