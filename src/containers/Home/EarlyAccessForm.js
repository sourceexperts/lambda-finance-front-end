import React, { Component } from 'react'
import { Form, Input, Button } from 'antd'

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class EarlyAccessForm extends Component {
  componentDidMount() {
    this.props.form.validateFields()
  }

  handleSubmit = (subscribe) => (e) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        subscribe({ variables: { email: values.email } })
      }
    })
  }

  render() {
    const {
      getFieldDecorator, getFieldsError, getFieldError, isFieldTouched,
    } = this.props.form

    const emailError = isFieldTouched('email') && getFieldError('email')

    return (
      <Form onSubmit={this.handleSubmit(this.props.subsribe)} className="early-access-form">
        <Form.Item
          validateStatus={emailError ? 'error' : ''}
          help={emailError || ''}
        >
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email' }],
          })(
            <Input size="large" placeholder="Enter your email address" />
          )}
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            size="large"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
            block
          >
            Submit
          </Button>
        </Form.Item>
      </Form >
    )
  }
}

const EarlyAccessFormWrapper = Form.create({ name: 'earlyaccess-form' })(EarlyAccessForm)

export default EarlyAccessFormWrapper;
