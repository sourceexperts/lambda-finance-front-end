import React from 'react'
import { Link } from 'react-router-dom'

import Logo from './Logo.js'

export default function Header(props) {
  return (
    <header {...props} id="header">
      <Logo />
      <div>
        <Link to="/login" style={{ fontSize: '18px' }}>Login</Link>
      </div>
    </header>
  );
}
