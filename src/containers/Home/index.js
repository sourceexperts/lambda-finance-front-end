import React from 'react'
import { enquireScreen } from 'enquire-js'
import Header from './Header'
import Banner from './Banner'
import Features from './Features'
import LambdaCode from './LambdaCode'
import EarlyAccess from './EarlyAccess'
import Footer from './Footer'
import './static/style'

let isMobile = false
enquireScreen((b) => {
  isMobile = b
})

class Home extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      isMobile,
    }
  }

  componentDidMount() {
    enquireScreen((b) => {
      this.setState({
        isMobile: !!b,
      })
    })
  }

  render() {
    const { isMobile } = this.state
    return (
      <div className="page-wrapper home">
        <Header />
        <Banner isMobile={isMobile} />
        <Features isMobile={isMobile} />
        <LambdaCode isMobile={isMobile} />
        <EarlyAccess />
        <Footer />
      </div>
    )
  }
}

export default Home
