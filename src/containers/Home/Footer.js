import React from 'react'
import { Row, Col } from 'antd'

function Footer() {
  return (
    <footer id="footer" className="dark">
      <div className="footer-wrap">
        <Row>
          <Col md={6} sm={24} xs={24}>
            <div className="footer-center">
              <h2>Company</h2>
              <div>
                <a target="_blank" href="/">
                  About
                </a>
              </div>
              <div>
                <a href="/">Blog</a>
              </div>
              <div>
                <a href="/">Careers</a>
              </div>
            </div>
          </Col>
          <Col md={6} sm={24} xs={24}>
            <div className="footer-center">
              <h2>Social</h2>
              <div>
                <a href="/changelog">
                  Facebook
                </a>
              </div>
              <div>
                <a target="_blank" rel="noopener noreferrer" href="/">
                  Twitter
                </a>
              </div>
              <div>
                <a target="_blank" rel="noopener noreferrer" href="/">
                  Github
                </a>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <Row className="bottom-bar">
        <Col md={4} sm={24} />
        <Col md={20} sm={24}>
          <span
            style={{
              lineHeight: '16px', paddingRight: 12, marginRight: 11, borderRight: '1px solid rgba(255, 255, 255, 0.55)',
            }}
          >
            <a
              href="/"
              rel="noopener noreferrer"
              target="_blank"
            >
              Privacy policy
            </a>
          </span>
          <span style={{ marginRight: 24 }}>
            <a
              href="/"
              rel="noopener noreferrer"
              target="_blank"
            >
              Terms and conditions
            </a>
          </span>
          <span style={{ marginRight: 12 }}>Copyright © lambda.finance</span>
        </Col>
      </Row>
    </footer>
  );
}

export default Footer;
