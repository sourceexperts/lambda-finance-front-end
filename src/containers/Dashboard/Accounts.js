import React from 'react'
import { Breadcrumb, Card } from 'antd'
import { useQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'

const GET_ACCOUNTS = gql`
    query accounts{
        accounts(bankId: 1){
            accounts { iban, currencyCode, currentBalance, productDescription}
        }
    }
`

export const Accounts = () => {
    const { data, loading } = useQuery(GET_ACCOUNTS)

    if (loading) {
        return 'Loading...'
    }

    return (
        <>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                <Breadcrumb.Item>My accounts</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
                {!data.accounts.accounts ?
                    'No accounts here. Please create one.' :
                    data.accounts.accounts.map(account => (
                    <Card
                        key={account.iban}
                        title={`${account.currencyCode} ${account.currentBalance}`}

                    >
                        <Card.Meta title={account.productDescription} description={account.iban} />
                    </Card>
                ))}
            </div>
        </>
    )
}
