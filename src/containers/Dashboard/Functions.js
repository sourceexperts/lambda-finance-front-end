import React, { useState, useEffect } from 'react'
import { Breadcrumb, Card, Icon, message } from 'antd'
import { useQuery, useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-github";

const GET_FUNCTIONS = gql`
  query myCodeSnippets {
    codeSnippets {
      id, name, code
    }
  }
`;

const UPDATE_FUNCTION = gql`
    mutation updateCodeSnippet($snippetId: ID!, $codeSnippet: CodeSnippetInput!) {
        updateCodeSnippet(
            snippetId: $snippetId,
            codeSnippet: $codeSnippet
        ) {
            id, name, code
        }
    }
`;

const EXECUTE_FUNCTION = gql`
    mutation executeCodeSnippet($snippetId: ID!) {
        executeCodeSnippet(snippetId: $snippetId)
    }
`;

const Snippet = ({ content }) => {
    const [code, setCode] = useState(content.code)
    const [update, { data: dataUpdate }] = useMutation(UPDATE_FUNCTION)
    const [execute, { data: dataExecute }] = useMutation(EXECUTE_FUNCTION) 
    const codeSnippet = { name: content.name, code }

    useEffect(() => {
        if (dataUpdate) {
            message.success('Update successful')
        }
    }, [dataUpdate])

     useEffect(() => {
        if (dataExecute) {
            message.success('Function run successfully')
        }
    }, [dataExecute])

    return (
        <Card
            title={content.name}
            actions={[
                <Icon type="check" key="check" onClick={() => update({ variables: { snippetId: content.id, codeSnippet }})} />,
                <Icon type="play-square" key="run" onClick={() => execute({ variables: { snippetId: content.id }})} />,
            ]}
            style={{marginBottom: '16px'}}
        >
            <AceEditor
                mode="javascript"
                theme="github"
                onChange={(text) => setCode(text)}
                value={code}
                name={`editor-${content.id}`}
                style={{ height: '200px' }}
                editorProps={{ $blockScrolling: true }}
            />
        </Card>
    )
}

export const Functions = () => {
    const { data, loading } = useQuery(GET_FUNCTIONS)

    if (loading) {
        return 'Loading...'
    }

    return (
        <>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                <Breadcrumb.Item>My functions</Breadcrumb.Item>
            </Breadcrumb>
            <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
                {data && data.codeSnippets.map(func => (
                    <Snippet key={func.id} content={func} />
                ))}
            </div>
        </>
    )
}
