import React, { useState } from 'react'
import { Layout, Menu, Icon } from 'antd'
import { Route, Link } from 'react-router-dom'

import { Functions } from './Functions'
import { Accounts } from './Accounts'
import { Triggers } from './Triggers'

const { Header, Content, Footer, Sider } = Layout;

const Dashboard = () => {
  const [collapsed, setCollapsed] = useState(false)
  const [logo, setLogo] = useState('Lambda.Finance')

  const onCollapse = collapsedValue => {
    setCollapsed(collapsedValue)
    setLogo(collapsedValue ? 'LF' : 'Lambda.Finance')
  }

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
        <div className="logo">{logo}</div>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
          <Menu.Item key="1">
            <Icon type="codepen" />
            <span><Link to="/dashboard/functions" style={{color: 'white'}}>
              Lambdas
            </Link></span>
          </Menu.Item>
          <Menu.Item key="2">
            <Icon type="credit-card" />
            <span><Link to="/dashboard/accounts" style={{color: 'white'}}>
              My accounts
            </Link></span>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header style={{ background: '#fff', padding: 0 }} />
        <Content style={{ margin: '0 16px' }}>
          <Route component={Functions} path="/dashboard/functions" />
          <Route component={Accounts} path="/dashboard/accounts" />
          <Route component={Triggers} path="/dashboard/triggers" />
        </Content>
        <Footer style={{ textAlign: 'center' }}>Lambda Finance © 2019</Footer>
      </Layout>
    </Layout>
  );
}

export default Dashboard;
