#!/usr/bin/sh
# TODO migrate to docker-compose
# docker run --detach \
#     --name nginx-proxy \
#     --publish 80:80 \
#     --publish 443:443 \
#     --volume /etc/nginx/certs \
#     --volume /etc/nginx/vhost.d \
#     --volume /usr/share/nginx/html \
#     --volume /var/run/docker.sock:/tmp/docker.sock:ro \
#     jwilder/nginx-proxy

# docker run --detach \
#     --name nginx-proxy-letsencrypt \
#     --volumes-from nginx-proxy \
#     --volume /var/run/docker.sock:/var/run/docker.sock:ro \
#     jrcs/letsencrypt-nginx-proxy-companion

docker login registry.gitlab.com -u lyamkin -p $TOKEN
docker pull registry.gitlab.com/sourceexperts/lambda-finance-front-end
docker stop lambdafinance-frontend || true

docker run --name lambdafinance-frontend --rm -d \
--env "VIRTUAL_HOST=lambda.finance" \
--env "LETSENCRYPT_HOST=lambda.finance" \
--env "LETSENCRYPT_EMAIL=ilya.lyamkin@gmail.com" \
registry.gitlab.com/sourceexperts/lambda-finance-front-end